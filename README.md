# AMIV API scripts

This repository contains several helper scripts to interact with the AMIV API. Below is a brief summary of each script:

## amivapi-event-import.py

This script reads in users from a CSV file with fields `FIRSTNAME`, `NAME`, `EMAIL` and `LEGI`. It defines helper functions to find a user in the database given the information in the CSV file. If the user is found, it is added to an event. Otherwise, the user is added to a list of unknown users.

## amivapi-eventsignup-automation.py

This script creates event signups for an event over the AMIV API.

## amivapi-groupmemberships-import.py

This script reads in a CSV file of users defined with a field `NETHZ`. If a user is found in the AMIV API, their group ID is set to the value of the argument `group_id`.

## amivapi-member-import.py

This script reads users from a CSV file with fields `firstname` and `lastname`. Then it sends a post request to the AMIV API to create a new user item for each user in the CSV file.

## amivapi-password-change.py

This script prompts the user to enter a `username`, current password, and new password twice. Then, it sends a post request to the AMIV API to change the user's password.

## amivapi-user-create.py

This script creates 20 test users in the AMIV API.

## amivapi-user-lookup.py

This script reads a CSV file with a `nethz` field and writes all information on those users in the database to the specified `outfile`.

## amivapi-user-retreive.py

This script reads out all information on all users in the database and writes them to the specified `outfile`.

## amivapi-add-eventsDB-fields.ipynb

This notebook can be used to add fields to the `/events` resource. The script iterates once over all events and uses patch requests to write new fields to the desired default value. Furthermore, the notebook contains methods to populate the database with random entries which were used to test the migration.

## amivapi-dev-setup.ipynb

This notebook sets up the amivapi so it can interface with a locally running amiv-admintool instance.
It creates a test admin group and a test admin user and adds the user to the group. It also creates a oauth client entry for the admin tool in the database.

## amivapi-populate-db.ipynb

This notebook can be used to populate the database with random entries. It contains methods to create random users and events.


Please refer to the documentation in each script for more details on usage and requirements.
