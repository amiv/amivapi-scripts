#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Author: Luzian Bieri <l.j.bieri@gmail.com>

import argparse
from builtins import list, type

import requests
import csv
import json


def request(headers, url, page=1):
    query = url + "?where={\"membership\":\"regular\"}&page=" + str(page) #extraordinary
    print(query)
    r = requests.get(query, headers=headers)
    if r.status_code != 200:
        print("Error ocurred")

    return r


parser = argparse.ArgumentParser(prog='amivapi-user-lookup.py',
                                 description='Returns all infos to all users (needed to send out survey)')
parser.add_argument('api_domain', nargs='?', default='api.amiv.ethz.ch',
                    help='Domain under which the AMIVAPI is accessible default: api.amiv.ethz.ch')
parser.add_argument('api_token', nargs='?', default='rootr', help='authorization token for the AMIVAPI, default: root')
parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'),
                    default='out.csv', help='file where the output is stored, default: out.csv')
args = parser.parse_args()

url = 'https://' + ''.join(args.api_domain) + '/users'
headers = {'Authorization': ''.join(args.api_token)}

writer = {}
first = True
max_pages = int(json.loads(request(headers, url).content)["_meta"]["total"]/25) + 1
print(max_pages)

for nr in range(1, max_pages + 1):
    res = json.loads(request(headers, url, nr).content)['_items']

    if len(res) == 0:
        print('WARNING: skipping ' + str(nr) + ' as response is null')
        continue

    if first:
        writer = csv.DictWriter(args.outfile, fieldnames=res[0].keys())
        first = False

    for row in res:
        writer.writerow(row)
