#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Author: Luzian Bieri <l.j.bieri@gmail.com>

import argparse
from builtins import list, type

import requests
import csv
import json


def request(field):
    print('------------------------------------')
    print('Requesting ' + field['nethz'])
    query = url + "{\"nethz\":\"" + field['nethz'] + "\"}"
    print(query)
    r = requests.get(query, headers=headers)
    print('Response: ' + r.text)

    if r.status_code != 200:
        print("Error ocurred")

    return r


parser = argparse.ArgumentParser(prog='amivapi-user-lookup.py',
                                 description='Returns all infos to a csv of netzh-kuerzeln')
parser.add_argument('api_domain', nargs='?', default='api.amiv.ethz.ch',
                    help='Domain under which the AMIVAPI is accessible default: api.amiv.ethz.ch')
parser.add_argument('api_token', nargs='?', default='root', help='authorization token for the AMIVAPI, default: root')
parser.add_argument('file', nargs='?', type=argparse.FileType('r'),
                    default='members.csv', help='file to import netzh-kuerzel from, default: members.csv')
parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'),
                    default='out.csv', help='file where the output is stored, default: out.csv')
args = parser.parse_args()

# Read member import file
if (type(args.file) is list):
    reader = csv.DictReader(args.file[-1], fieldnames=["nethz"])
else:
    reader = csv.DictReader(args.file, fieldnames=["nethz"])

url = 'https://' + ''.join(args.api_domain) + '/users?where='
headers = {'Authorization': ''.join(args.api_token)}

writer = {}
first = True

for row in reader:
    res = json.loads(request(row).content)['_items']

    if len(res) == 0:
        print('WARNING: skipping ' + json.dumps(row) + ' as response is null')
        continue

    res = res[0]
    if first:
        writer = csv.DictWriter(args.outfile, fieldnames=res.keys())
        first = False

    writer.writerow(res)
